<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8" />
        <title>Création d'un client</title>
        <link type="text/css" rel="stylesheet" href="<c:url value="/inc/style.css"/>" />
    </head>
    <body>
    	<c:import url="/WEB-INF/inc/menu.jsp" />
        <div>
        	<form:form method="POST" action="createCustomer" modelAttribute="customerForm">
                <fieldset>
                    <legend>Informations client</legend>
                    <c:import url="/WEB-INF/inc/inc_customer_form.jsp" />
                </fieldset>
                <p class="info">${form.result}</p>
                <form:button type="submit" class="submitButton">Valider</form:button>
                <form:button type="reset" class="resetButton">Remettre à zéro</form:button>
                <br/>
            </form:form>
        </div>
    </body>
</html>