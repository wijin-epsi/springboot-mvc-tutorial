<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="fr">
<head>
<meta charset="utf-8" />
<title>Liste des clients existants</title>
<link type="text/css" rel="stylesheet"
	href="/inc/style.css" />
</head>
<body>
	<c:import url="/WEB-INF/inc/menu.jsp" />
	<div id="corps">
		<c:choose>
			<%-- Si aucun client n'existe en session, affichage d'un message par défaut --%>
			<c:when test="${empty sessionScope.customers}">
				<p class="error">Aucun client enregistré</p>
			</c:when>
			<%-- Sinon, affichage du tableau. --%>
			<c:otherwise>
				<table>
					<caption>Liste des clients</caption>
					<tr>
						<th>Nom</th>
						<th>Prénom</th>
						<th>Entreprise</th>
						<th>Téléphone</th>
						<th>Mobile</th>
						<th>Email</th>
						<th>Actif</th>
						<th class="action">Action</th>
					</tr>
					<%-- Parcours de la Map des clients en session, et utilisation de l'objet varStatus --%>
					<c:forEach items="${sessionScope.customers}" var="customersMap"
						varStatus="boucle">
						<%-- Simple test de parité sur l'index de parcours, pour alterner la couleur de fond de chaque ligne du tableau. --%>
						<tr class="${boucle.index % 2 == 0 ? 'pair' : 'impair'}">
							<%-- Affichage des propriétés du bean Customer, qui est stocké en tant que valeur de l'entrée courante de la map --%>
							<td><c:out value="${customersMap.value.lastname}" /></td>
							<td><c:out value="${customersMap.value.firstname}" /></td>
							<td><c:out value="${customersMap.value.company}" /></td>
							<td><c:out value="${customersMap.value.phone}" /></td>
							<td><c:out value="${customersMap.value.mobile}" /></td>
							<td><c:out value="${customersMap.value.mail}" /></td>
							<td><c:out value="${customersMap.value.active}" /></td>
							<%-- Lien vers la servlet de suppression, avec passage de l'id du client en paramètre grâce à la balise <c:param/> --%>
							<td class="action"><a
								href="<c:url value="/deleteCustomer"><c:param name="customerId" value="${customersMap.key}" /></c:url>">
									<img src="<c:url value="/inc/delete.png"/>" alt="Supprimer" />
							</a></td>
						</tr>
					</c:forEach>
				</table>
			</c:otherwise>
		</c:choose>
		<c:if test="${!empty sessionScope.error}">
			<p class="error">${sessionScope.error}</p>
		</c:if>
	</div>
</body>
</html>