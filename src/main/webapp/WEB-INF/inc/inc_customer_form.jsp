<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<form:label path="lastname">Nom <span class="required">*</span></form:label>
<form:input id="lastname" path="lastname" maxlength="100" />
<form:errors path="lastname" cssClass="error" />
<br />

<form:label path="firstname">Prénom <span class="required">*</span></form:label>
<form:input id="firstname" path="firstname" maxlength="100" />
<form:errors path="firstname" cssClass="error" />
<br />

<form:label path="company">Entreprise <span class="required">*</span></form:label>
<form:input id="company" path="company" maxlength="200" />
<form:errors path="company" cssClass="error" />
<br />

<form:label path="phone">Téléphone <span class="required">*</span></form:label>
<form:input id="phone" path="phone" maxlength="15" />
<form:errors path="phone" cssClass="error" />
<br />

<form:label path="mobile">Mobile <span class="required">*</span></form:label>
<form:input id="mobile" path="mobile" maxlength="15" />
<form:errors path="mobile" cssClass="error" />
<br />

<form:label path="mail">Adresse email <span class="required">*</span></form:label>
<form:input id="mail" path="mail" maxlength="255" />
<form:errors path="mail" cssClass="error" />
<br />

<form:label path="notes">Notes </form:label>
<form:textarea id="notes" path="notes" maxlength="255" cols="60" rows="4"></form:textarea>
<form:errors path="notes" cssClass="error" />
<br />

<form:label path="active">Actif </form:label>
<form:checkbox id="active" path="active"></form:checkbox>
<form:errors path="active" cssClass="error" />
<br />