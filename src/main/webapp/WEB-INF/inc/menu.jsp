<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div id="menu">
    <p><a href="<c:url value="/createCustomer"/>">Créer un nouveau client</a></p>
    <p><a href="<c:url value="/listCustomers"/>">Voir les clients existants</a></p>
 </div>