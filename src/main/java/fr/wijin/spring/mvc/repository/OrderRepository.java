package fr.wijin.spring.mvc.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import fr.wijin.spring.mvc.model.Order;

public interface OrderRepository extends JpaRepository<Order, Integer> {
	
	/**
	 * Get a list of orders for a type and a status
	 * @param type the type
	 * @param status the status
	 * @return a list of orders
	 */
	List<Order> findByTypeAndStatus(@Param("type") String type, @Param("status") String status);

}
