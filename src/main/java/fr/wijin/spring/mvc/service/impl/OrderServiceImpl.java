package fr.wijin.spring.mvc.service.impl;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.wijin.spring.mvc.model.Order;
import fr.wijin.spring.mvc.repository.OrderRepository;
import fr.wijin.spring.mvc.service.OrderService;

@Service
public class OrderServiceImpl implements OrderService {

	Logger logger = LoggerFactory.getLogger(OrderServiceImpl.class);

	@Autowired
	private OrderRepository orderRepository;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Order> getAllOrders() {
		return orderRepository.findAll();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Order getOrderById(Integer orderId) {
		Optional<Order> order = orderRepository.findById(orderId);
		return order.isEmpty() ? null : order.get();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Order createOrder(Order order) {
		return orderRepository.save(order);
	}
	

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Order updateOrder(Order order) throws Exception {
		logger.debug("attempting to update order {}...", order.getId());
		Order existingOrder = orderRepository.findById(order.getId()).orElseThrow(() -> new Exception("Aucune commande trouvée!"));
		existingOrder.setLabel(order.getLabel());
		existingOrder.setNumberOfDays(order.getNumberOfDays());
		existingOrder.setNotes(order.getNotes());
		existingOrder.setStatus(order.getStatus());
		existingOrder.setTva(order.getTva());
		existingOrder.setAdrEt(order.getAdrEt());
		existingOrder.setType(order.getType());
		return orderRepository.save(existingOrder);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteOrder(Integer id) throws Exception {
		Order existingOrder = orderRepository.findById(id).orElseThrow(() -> new Exception("Aucune commande trouvée!"));
		orderRepository.delete(existingOrder);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void patchOrderStatus(Integer orderId, String status) throws Exception {
		logger.debug("attempting to patch order {} with status = {}...", orderId, status);
		Order existingOrder = orderRepository.findById(orderId).orElseThrow(Exception::new);
		existingOrder.setStatus(status);
		orderRepository.save(existingOrder);
	}

}
