package fr.wijin.spring.mvc.service.impl;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.wijin.spring.mvc.model.Customer;
import fr.wijin.spring.mvc.repository.CustomerRepository;
import fr.wijin.spring.mvc.service.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService {

	Logger logger = LoggerFactory.getLogger(CustomerServiceImpl.class);

	@Autowired
	private CustomerRepository customerRepository;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Customer> getAllCustomers() {
		return customerRepository.findAll();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Customer getCustomerById(Integer customerId) {
		Optional<Customer> customer = customerRepository.findById(customerId);
		return customer.isEmpty() ? null : customer.get();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Customer> getCustomersByActive(Boolean active) {
		return customerRepository.findByActive(active);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Customer createCustomer(Customer customer) {
		return customerRepository.save(customer);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Customer updateCustomer(Customer customer) throws Exception {
		logger.debug("attempting to update customer {}...", customer.getId());
		Customer clientExistant = customerRepository.findById(customer.getId()).orElseThrow(() -> new Exception("Aucun client trouvé!"));
		clientExistant.setLastname(customer.getLastname());
		clientExistant.setFirstname(customer.getFirstname());
		clientExistant.setMail(customer.getMail());
		clientExistant.setCompany(customer.getCompany());
		clientExistant.setMobile(customer.getMobile());
		clientExistant.setNotes(customer.getNotes());
		clientExistant.setActive(customer.isActive());
		return customerRepository.save(clientExistant);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteCustomer(Integer customerId) throws Exception {
		Customer customerToDelete = customerRepository.findById(customerId).orElseThrow(() -> new Exception("Aucun client trouvé!"));
		customerRepository.delete(customerToDelete);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void patchCustomerStatus(Integer customerId, boolean active) throws Exception {
		logger.debug("attempting to patch customer {} with active = {}...", customerId, active);
		Customer existingCustomer = customerRepository.findById(customerId).orElseThrow(Exception::new);
		existingCustomer.setActive(active);
		customerRepository.save(existingCustomer);
	}

}
