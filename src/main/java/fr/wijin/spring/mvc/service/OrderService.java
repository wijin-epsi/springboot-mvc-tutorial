package fr.wijin.spring.mvc.service;

import java.util.List;

import fr.wijin.spring.mvc.model.Order;

public interface OrderService {
	
	/**
	 * Get all orders
	 * @return a list of orders
	 */
	List<Order> getAllOrders();
	
	/**
	 * Get an order by id
	 * @param id the id
	 * @return the order
	 */
	Order getOrderById(Integer id);
	
	/**
	 * Create an order
	 * @param order the order to create
	 * @return the order
	 */
	Order createOrder(Order order);
	
	/**
	 * Update an order
	 * @param order the order to update
	 * @return the order
	 * @throws Exception
	 */
	Order updateOrder(Order order) throws Exception;
	
	/**
	 * Delete an order
	 * @param id the id of the order to delete
	 */
	void deleteOrder(Integer id) throws Exception;
	
	/**
	 * Patch the status of an order
	 * @param orderId the id
	 * @param status the status to patch
	 * @throws Exception
	 */
	void patchOrderStatus(Integer orderId, String status) throws Exception;

}
