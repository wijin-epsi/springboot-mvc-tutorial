package fr.wijin.spring.mvc.service.impl;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.wijin.spring.mvc.model.User;
import fr.wijin.spring.mvc.repository.UserRepository;
import fr.wijin.spring.mvc.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

	@Autowired
	private UserRepository userRepository;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<User> getAllUsers() {
		return userRepository.findAll();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public User getUserById(Integer userId) {
		Optional<User> user = userRepository.findById(userId);
		return user.isEmpty() ? null : user.get();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public User createUser(User user) {
		return userRepository.save(user);
	}
	

	/**
	 * {@inheritDoc}
	 */
	@Override
	public User updateUser(User user) throws Exception {
		logger.debug("attempting to update user {}...", user.getId());
		User existingUser = userRepository.findById(user.getId()).orElseThrow(() -> new Exception("Aucun utilisateur trouvé!"));
		existingUser.setMail(user.getMail());
		existingUser.setUsername(user.getUsername());
		existingUser.setPassword(user.getPassword());
		existingUser.setGrants(user.getGrants());
		return userRepository.save(existingUser);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteUser(Integer id) throws Exception {
		User existingUser = userRepository.findById(id).orElseThrow(() -> new Exception("Aucun utilisateur trouvé!"));
		userRepository.delete(existingUser);
	}

}
