package fr.wijin.spring.mvc.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import fr.wijin.spring.mvc.form.CustomerForm;
import fr.wijin.spring.mvc.model.Customer;
import fr.wijin.spring.mvc.repository.CustomerRepository;

@Controller
public class CustomerController {

	@Autowired
	private CustomerRepository customerRepository;

	public static final String SESSION_CUSTOMERS = "customers";

	@GetMapping("/listCustomers")
	public String showForm(Model model, HttpSession session) {

		/*
		 * Si la Map des clients n'existe pas en session, alors l'utilisateur se
		 * connecte pour la première fois et il faut charger en session les informations
		 * contenues dans la BDD
		 */
		if (session.getAttribute(SESSION_CUSTOMERS) == null) {
			// Récupération de la liste des clients existants et enregistrement en session
			List<Customer> customersList = customerRepository.findAll();
			Map<Integer, Customer> clientsMap = new HashMap<>();
			for (Customer customer : customersList) {
				clientsMap.put(customer.getId(), customer);
			}
			session.setAttribute(SESSION_CUSTOMERS, clientsMap);
		}

		return "listCustomers";
	}

	@GetMapping("/createCustomer")
	public String showForm(CustomerForm customerForm, Model model) {
		model.addAttribute("customerForm", new CustomerForm());
		return "createCustomer";
	}

	@PostMapping("/createCustomer")
	public String checkCustomerInfo(@ModelAttribute("customerForm") @Validated CustomerForm customerForm,
			BindingResult bindingResult, Model model, HttpSession session) {

		if (!bindingResult.hasErrors()) {
			Customer customer = new Customer();
			customer.setFirstname(customerForm.getFirstname());
			customer.setLastname(customerForm.getLastname());
			customer.setCompany(customerForm.getCompany());
			customer.setMail(customerForm.getMail());
			customer.setPhone(customerForm.getPhone());
			customer.setMobile(customerForm.getMobile());
			customer.setNotes(customerForm.getNotes());
			customer.setActive(customerForm.getActive());
			customerRepository.save(customer);

			// Ajout du bean Customer et de l'objet métier form à la requête
			model.addAttribute("customer", customer);

			// Pas d'erreur : récupération de la Map des clients dans la session
			Map<Integer, Customer> customers = (HashMap<Integer, Customer>) session.getAttribute(SESSION_CUSTOMERS);

			if (customers == null) {
				// Initialisation d'une Map si rien en session
				customers = new HashMap<>();
			}
			// Ajout du client courant dans la Map
			customers.put(customer.getId(), customer);
			// Repositionnement de la Map en session
			session.setAttribute(SESSION_CUSTOMERS, customers);

			System.out.println("*** Pas d'erreur : redirection vers /listCustomers ***");
			return "redirect:/listCustomers";
			// return "listCustomers"; // Possible mais pas de redirection (on ne passe pas
			// par le controller)
		} else {
			// Affichage du formulaire avec les erreurs
			System.out.println("*** Erreur : redirection vers /createCustomers ***");
			return "createCustomer";
		}

	}

	@GetMapping("/deleteCustomer")
	public String deleteCustomer(@RequestParam("customerId") Integer id, Model model, HttpSession session) {
		try {
			Optional<Customer> customer = customerRepository.findById(id);
			if (customer.isPresent()) {
				customerRepository.delete(customer.get());

				Map<Integer, Customer> customers = (HashMap<Integer, Customer>) session.getAttribute(SESSION_CUSTOMERS);
				// Suppression du client de la Map
				customers.remove(id);
				session.removeAttribute("error");
			}
		} catch (Exception e) {
			session.setAttribute("error", "Impossible de supprimer le client!");
		}
		return "redirect:/listCustomers";
	}

}
