package fr.wijin.spring.mvc.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import fr.wijin.spring.mvc.form.CustomerForm;
import fr.wijin.spring.mvc.model.Customer;
import fr.wijin.spring.mvc.repository.CustomerRepository;

@RestController
public class CustomerRestController {
	
	@Autowired
	private CustomerRepository customerRepository;	

	@GetMapping("/customers/{id}")
	public CustomerForm customers(@PathVariable(value = "id") Integer id) {
		Optional<Customer> optionalCustomer = customerRepository.findById(id);
		
		CustomerForm customerForm = new CustomerForm();
		if (optionalCustomer.isPresent()) {
			Customer customer = optionalCustomer.get();
			customerForm.setFirstname(customer.getFirstname());
			customerForm.setLastname(customer.getLastname());
			customerForm.setCompany(customer.getCompany());
			customerForm.setMail(customer.getMail());
			customerForm.setPhone(customer.getPhone());
			customerForm.setMobile(customer.getMobile());
			customerForm.setNotes(customer.getNotes());
			customerForm.setActive(customer.isActive());
		}
		return customerForm;
	}

}
