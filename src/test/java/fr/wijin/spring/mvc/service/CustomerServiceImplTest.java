package fr.wijin.spring.mvc.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import fr.wijin.spring.mvc.model.Customer;
import fr.wijin.spring.mvc.repository.CustomerRepository;
import fr.wijin.spring.mvc.service.impl.CustomerServiceImpl;

@ExtendWith(MockitoExtension.class)
class CustomerServiceImplTest {
	
	@InjectMocks
	private CustomerServiceImpl customerService;
	
	@Mock
	private CustomerRepository customerRepository;

	@BeforeEach
    public void init() {
        MockitoAnnotations.openMocks(this);
    }
	
	@Test
	void testGetAll() {
		List<Customer> customers = new ArrayList<>();
		customers.add(new Customer(99, "test", "test", "test", "test", "test", "test", "test", true));
		Mockito.when(customerRepository.findAll()).thenReturn(customers);

		List<Customer> customersRecup = customerService.getAllCustomers();
		assertEquals(1, customersRecup.size());
	}
	
	@Test
	void testGetById() {
		Optional<Customer> customerTest = Optional.of(new Customer(99, "test", "test", "test", "test", "test", "test", "test", true));
		Mockito.when(customerRepository.findById(99)).thenReturn(customerTest);
		Customer customer = customerService.getCustomerById(Integer.valueOf(99));
		assertNotNull(customer);
		assertEquals(99, customer.getId());
	}
	
	@Test
	void testCreate() {
		Customer customer = new Customer();
		customer.setLastname("MARTIN");
		Mockito.when(customerRepository.save(customer)).thenReturn(customer);
		Customer createdCustomer = customerService.createCustomer(customer);
		assertNotNull(createdCustomer);
	}
	
	@Test
	void testUpdate() {
		Customer customer = new Customer();
		customer.setId(Integer.valueOf(1));
		customer.setFirstname("Bob");
		Optional<Customer> customerTest = Optional.of(customer);
		Mockito.when(customerRepository.findById(1)).thenReturn(customerTest);
		Mockito.when(customerRepository.save(customer)).thenReturn(customer);
		try {
			customerService.updateCustomer(customer);
		} catch (Exception e){
			fail();
		}
	}
	
	@Test
	void testUpdateKO() {
		Customer customer = new Customer();
		customer.setId(Integer.valueOf(1));
		customer.setFirstname("Bob");
		Optional<Customer> customerTest = Optional.empty();
		Mockito.when(customerRepository.findById(1)).thenReturn(customerTest);
		try {
			customerService.updateCustomer(customer);
			fail();
		} catch (Exception e){
			assertEquals("Aucun client trouvé!", e.getMessage());
		}
		Mockito.verify(customerRepository, Mockito.times(0)).save(customer);
	}
	
	@Test
	void testDelete() {
		Customer customer = new Customer();
		customer.setId(Integer.valueOf(3));
		Optional<Customer> customerTest = Optional.of(customer);
		
		Mockito.when(customerRepository.findById(3)).thenReturn(customerTest);
		Mockito.doNothing().when(customerRepository).delete(customer);
		
		try {
			customerService.deleteCustomer(3);
		} catch (Exception e){
			fail();
		}
		Mockito.verify(customerRepository, Mockito.times(1)).delete(customer);
	}
	
	@Test
	void testDeleteKO() {
		Customer customer = new Customer();
		customer.setId(Integer.valueOf(3));
		Optional<Customer> customerTest = Optional.empty();
		
		Mockito.when(customerRepository.findById(3)).thenReturn(customerTest);
		
		try {
			customerService.deleteCustomer(3);
			fail();
		} catch (Exception e){
			assertEquals("Aucun client trouvé!", e.getMessage());
		}
		Mockito.verify(customerRepository, Mockito.times(0)).delete(customer);
	}
	
}
