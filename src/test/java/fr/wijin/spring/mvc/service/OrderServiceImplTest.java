package fr.wijin.spring.mvc.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import fr.wijin.spring.mvc.model.Order;
import fr.wijin.spring.mvc.repository.OrderRepository;
import fr.wijin.spring.mvc.service.impl.OrderServiceImpl;

@ExtendWith(MockitoExtension.class)
class OrderServiceImplTest {
	
	@InjectMocks
	private OrderServiceImpl orderService;

	@Mock
	private OrderRepository orderRepository;
	
	@BeforeEach
    public void init() {
        MockitoAnnotations.openMocks(this);
    }
	
	@Test
	void testGetAllOrders() {
		List<Order> orders = new ArrayList<>();
		orders.add(new Order(99, "test", 9.0, 1.0, 1.0, "test", "test", "test", null));
		Mockito.when(orderRepository.findAll()).thenReturn(orders);
		List<Order> ordersRecup = orderService.getAllOrders();
		assertEquals(1, ordersRecup.size());
	}
	
	@Test
	void testGetOrderById() {
		Optional<Order> orderTest = Optional.of(new Order(99, "test", 1.0, 1.0, 1.0, "test", "test", "test", null));
		Mockito.when(orderRepository.findById(99)).thenReturn(orderTest);
		Order order = orderService.getOrderById(Integer.valueOf(99));
		assertNotNull(order);
		assertEquals(99, order.getId());
	}
	
	@Test
	void testCreate() {
		Order order = new Order();
		order.setLabel("Libellé");
		Mockito.when(orderRepository.save(order)).thenReturn(order);
		Order createdOrder = orderService.createOrder(order);
		assertNotNull(createdOrder);
	}
	
	@Test
	void testUpdate() {
		Order order = new Order();
		order.setId(Integer.valueOf(1));
		order.setLabel("Nouveau libellé");
		
		Optional<Order> orderTest = Optional.of(order);
		Mockito.when(orderRepository.findById(1)).thenReturn(orderTest);
		Mockito.when(orderRepository.save(order)).thenReturn(order);
		try {
			orderService.updateOrder(order);
		} catch (Exception e){
			fail();
		}
	}
	
	@Test
	void testUpdateKO() {
		Order order = new Order();
		order.setId(Integer.valueOf(1));
		order.setLabel("Nouveau libellé");
		
		Optional<Order> orderTest = Optional.empty();
		Mockito.when(orderRepository.findById(1)).thenReturn(orderTest);
		try {
			orderService.updateOrder(order);
			fail();
		} catch (Exception e){
			assertEquals("Aucune commande trouvée!", e.getMessage());
		}
	}
	
	@Test
	void testDelete() {
		Order order = new Order();
		order.setId(Integer.valueOf(3));
		order.setLabel("Nouveau libellé");
		Optional<Order> orderTest = Optional.of(order);
		
		Mockito.when(orderRepository.findById(3)).thenReturn(orderTest);
		Mockito.doNothing().when(orderRepository).delete(order);

		try {
			orderService.deleteOrder(3);
		} catch (Exception e){
			fail();
		}
	}
	
	@Test
	void testDeleteKO() {
		Order order = new Order();
		order.setId(Integer.valueOf(3));
		order.setLabel("Nouveau libellé");
		Optional<Order> orderTest = Optional.empty();
		
		Mockito.when(orderRepository.findById(3)).thenReturn(orderTest);

		try {
			orderService.deleteOrder(3);
			fail();
		} catch (Exception e){
			assertEquals("Aucune commande trouvée!", e.getMessage());
		}
	}
	
}
