package fr.wijin.spring.mvc.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import fr.wijin.spring.mvc.model.User;
import fr.wijin.spring.mvc.repository.UserRepository;
import fr.wijin.spring.mvc.service.impl.UserServiceImpl;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {
	
	@InjectMocks
	private UserServiceImpl userService;
	
	@Mock
	private UserRepository userRepository;
	
	@BeforeEach
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

	@Test
	void testGetAllUsers() {
		List<User> users = new ArrayList<>();
		users.add(new User(99, "test", "test", "test", "test"));
		Mockito.when(userRepository.findAll()).thenReturn(users);
		List<User> usersRecup = userService.getAllUsers();
		assertEquals(1, usersRecup.size());
	}
	
	@Test
	void testGetUserById() {
		Optional<User> userTest = Optional.of(new User(1, "test", "test", "test", "test"));
		Mockito.when(userRepository.findById(1)).thenReturn(userTest);
		
		User user = userService.getUserById(Integer.valueOf(1));
		assertNotNull(user);
		assertEquals(1, user.getId());
	}
	
	@Test
	void testCreate() {
		User user = new User();
		user.setMail("monmail@test.fr");
		Mockito.when(userRepository.save(user)).thenReturn(user);
		User createdUser = userService.createUser(user);
		assertNotNull(createdUser);
	}
	
	@Test
	void testUpdate() {
		User user = new User();
		user.setId(Integer.valueOf(1));
		user.setMail("mailmodifie@test.fr");
		Optional<User> userTest = Optional.of(user);
		Mockito.when(userRepository.findById(1)).thenReturn(userTest);
		Mockito.when(userRepository.save(user)).thenReturn(user);
		try {
			userService.updateUser(user);
		} catch (Exception e){
			fail();
		}
	}
	
	@Test
	void testUpdateKO() {
		User user = new User();
		user.setId(Integer.valueOf(1));
		user.setMail("mailmodifie@test.fr");
		Optional<User> userTest = Optional.empty();
		Mockito.when(userRepository.findById(1)).thenReturn(userTest);
		try {
			userService.updateUser(user);
			fail();
		} catch (Exception e){
			assertEquals("Aucun utilisateur trouvé!", e.getMessage());
		}
	}
	
	@Test
	void testDelete() {
		User user = new User();
		user.setId(Integer.valueOf(2));
		user.setMail("mail@test.fr");
		Optional<User> userTest = Optional.of(user);
		Mockito.when(userRepository.findById(2)).thenReturn(userTest);
		Mockito.doNothing().when(userRepository).delete(user);
		try {
			userService.deleteUser(2);
		} catch (Exception e){
			fail();
		}
	}
	
	@Test
	void testDeleteKO() {
		User user = new User();
		user.setId(Integer.valueOf(2));
		user.setMail("mail@test.fr");
		Optional<User> userTest = Optional.empty();
		Mockito.when(userRepository.findById(2)).thenReturn(userTest);
		try {
			userService.deleteUser(2);
			fail();
		} catch (Exception e){
			assertEquals("Aucun utilisateur trouvé!", e.getMessage());
		}
	}
	
}
