package fr.wijin.spring.mvc.repository;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import fr.wijin.spring.mvc.model.Order;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@TestMethodOrder(OrderAnnotation.class)
class OrderRepositoryImplTest {

	Logger logger = LoggerFactory.getLogger(OrderRepositoryImplTest.class);

	public static final String LABEL = "Formation Java";
	public static final String LASTNAME = "JONES";

	@Autowired
	private OrderRepository orderRepository;

	@Test
	@org.junit.jupiter.api.Order(1)
	void testFindById() {
		Optional<Order> order = orderRepository.findById(1);
		logger.debug("Commande = {}", order.get());
		Assertions.assertTrue(order.isPresent(), "Order not found");
	}

	@Test
	@org.junit.jupiter.api.Order(2)
	void testFindById2() {
		Optional<Order> order = orderRepository.findById(1);
		Assertions.assertEquals(LABEL, order.get().getLabel(), "Order label should be " + LABEL);
	}

	@Test
	@org.junit.jupiter.api.Order(3)
	void testFindById3() {
		Optional<Order> order = orderRepository.findById(1);
		Assertions.assertEquals(LASTNAME, order.get().getCustomer().getLastname(),
				"Customer lastname should be " + LASTNAME);
	}

	@Test
	@org.junit.jupiter.api.Order(4)
	void testFindAll() {
		List<Order> orders = orderRepository.findAll();
		Assertions.assertEquals(3, orders.size(), "Wrong number of orders");
	}

	@Test
	@org.junit.jupiter.api.Order(5)
	void testFindByTypeAndStatus() {
		List<Order> orders = orderRepository.findByTypeAndStatus("Forfait", "En attente");
		Assertions.assertEquals(1, orders.size(), "Wrong number of orders for type and status");
	}

	@Test
	@org.junit.jupiter.api.Order(6)
	void testCreation() {
		Order newOrder = new Order();
		newOrder.setLabel(LABEL);
		newOrder.setNumberOfDays(Double.valueOf(5));
		newOrder.setAdrEt(Double.valueOf(350));
		newOrder.setTva(Double.valueOf(20.0));
		newOrder.setType("Super commande");
		newOrder.setStatus("En cours");
		newOrder.setNotes("Les notes sur la commande");

		List<Order> orders = orderRepository.findAll();
		int numberOfOrdersBeforeCreation = orders.size();

		orderRepository.save(newOrder);

		List<Order> ordersAfterCreation = orderRepository.findAll();
		int numberOfOrdersAfterCreation = ordersAfterCreation.size();
		Assertions.assertEquals(numberOfOrdersBeforeCreation + 1, numberOfOrdersAfterCreation);
	}

	@Test
	@org.junit.jupiter.api.Order(7)
	void testUpdate() {
		Optional<Order> order = orderRepository.findById(2);
		order.get().setLabel("Nouveau libellé");

		orderRepository.save(order.get());

		Optional<Order> updatedOrder = orderRepository.findById(2);
		Assertions.assertEquals("Nouveau libellé", updatedOrder.get().getLabel());
	}

	@Test
	@org.junit.jupiter.api.Order(8)
	void testDelete() {
		Optional<Order> order = orderRepository.findById(2);

		orderRepository.delete(order.get());

		Optional<Order> deletedOrder = orderRepository.findById(2);
		Assertions.assertTrue(deletedOrder.isEmpty(), "Deleted order must be null");
	}

}
