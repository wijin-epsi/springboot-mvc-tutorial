package fr.wijin.spring.mvc.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class SpringMvcValidatorApplicationTests {

	public static final String GET_CUSTOMERS_URI = "/listCustomers";
	public static final String CREATE_CUSTOMER_URI = "/createCustomer";
	public static final String FIRST_NAME = "John";
	public static final String LAST_NAME = "Doe";
	public static final String COMPANY = "Entreprise";
	public static final String MAIL = "mail@mail.com";
	public static final String PHONE = "0202020202";
	public static final String MOBILE = "0606060606";
	public static final String NOTES = "Les notes";
	public static final String ACTIVE = "true";

	@Autowired
	private MockMvc mockMvc;

	@Test
	void checkListCustomersThenSuccess() throws Exception {
		MockHttpServletRequestBuilder listCustomers = get(GET_CUSTOMERS_URI);

		mockMvc.perform(listCustomers).andExpect(model().hasNoErrors());
	}

	@Test
	void checkCustomerInfoWhenFirstnameMissingNameThenFailure() throws Exception {
		MockHttpServletRequestBuilder createCustomer = post(CREATE_CUSTOMER_URI).param("lastname", LAST_NAME)
				.param("company", COMPANY).param("mail", MAIL).param("phone", PHONE).param("mobile", MOBILE)
				.param("notes", NOTES).param("active", ACTIVE);

		mockMvc.perform(createCustomer).andExpect(model().hasErrors());
	}

	@Test
	void checkCustomerInfoWhenLastnameMissingNameThenFailure() throws Exception {
		MockHttpServletRequestBuilder createCustomer = post(CREATE_CUSTOMER_URI).param("firstname", FIRST_NAME)
				.param("company", COMPANY).param("mail", MAIL).param("phone", PHONE).param("mobile", MOBILE)
				.param("notes", NOTES).param("active", ACTIVE);

		mockMvc.perform(createCustomer).andExpect(model().hasErrors());
	}

	@Test
	void checkCustomerInfoWhenFirstnameTooShortThenFailure() throws Exception {
		MockHttpServletRequestBuilder createCustomer = post(CREATE_CUSTOMER_URI).param("firstname", "A")
				.param("lastname", LAST_NAME).param("company", COMPANY).param("mail", MAIL).param("phone", PHONE)
				.param("mobile", MOBILE).param("notes", NOTES).param("active", ACTIVE);

		mockMvc.perform(createCustomer).andExpect(model().hasErrors());
	}

	@Test
	void checkCustomerInfoWhenLastnameTooShortThenFailure() throws Exception {
		MockHttpServletRequestBuilder createCustomer = post(CREATE_CUSTOMER_URI).param("firstname", FIRST_NAME)
				.param("lastname", "A").param("company", COMPANY).param("mail", MAIL).param("phone", PHONE)
				.param("mobile", MOBILE).param("notes", NOTES).param("active", ACTIVE);

		mockMvc.perform(createCustomer).andExpect(model().hasErrors());
	}

	@Test
	void checkCustomerInfoWhenCompanyMissingThenFailure() throws Exception {
		MockHttpServletRequestBuilder createCustomer = post(CREATE_CUSTOMER_URI).param("firstname", FIRST_NAME)
				.param("lastname", LAST_NAME).param("mail", MAIL).param("phone", PHONE).param("mobile", MOBILE)
				.param("notes", NOTES).param("active", ACTIVE);

		mockMvc.perform(createCustomer).andExpect(model().hasErrors());
	}

	@Test
	void checkCustomerInfoWhenPhoneTooShortThenFailure() throws Exception {
		MockHttpServletRequestBuilder createCustomer = post(CREATE_CUSTOMER_URI).param("firstname", FIRST_NAME)
				.param("lastname", LAST_NAME).param("company", COMPANY).param("mail", MAIL).param("phone", "030303")
				.param("mobile", MOBILE).param("notes", NOTES).param("active", ACTIVE);

		mockMvc.perform(createCustomer).andExpect(model().hasErrors());
	}

	@Test
	void checkCustomerInfoWhenValidRequestThenSuccess() throws Exception {
		MockHttpServletRequestBuilder createCustomer = post(CREATE_CUSTOMER_URI).param("firstname", FIRST_NAME)
				.param("lastname", LAST_NAME).param("company", COMPANY).param("mail", MAIL).param("phone", PHONE)
				.param("mobile", MOBILE).param("notes", NOTES).param("active", ACTIVE);

		mockMvc.perform(createCustomer).andExpect(model().hasNoErrors());
	}

}
