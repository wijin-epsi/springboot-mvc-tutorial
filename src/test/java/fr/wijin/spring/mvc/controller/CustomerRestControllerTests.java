package fr.wijin.spring.mvc.controller;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
//@WebMvcTest(CustomerRestController.class) //--> pour instancier uniquement la couche Web et pas tout le contexte + spécifier le controller
class CustomerRestControllerTests {
	
	@Autowired
	private MockMvc mockMvc;

	@Test
	void CustomersGetShouldReturnTailoredMessage() throws Exception {

		this.mockMvc.perform(get("/customers/1"))
				.andDo(print()).andExpect(status().isOk())
				.andExpect(content().string(containsString("Indiana")));
	}


}
